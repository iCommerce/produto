Serviço consultar produtos Loja e-Books

Para cada consulta realizada, campos retornados serão:

id 	(long), 
nome 	(String[255]), 
descrição (String[255]),
categoria (String[255]),
preço (double).

===========================================================
MAPEAMENTOS para as consultas:

			/produtos

mostra TODOS os produtos cadastrados na base.

Exemplo de chamada: 
/produto (Metodo GET).

retorno:
[
    {
        "nome": "Linguagem C. Completa e Descomplicada",
        "descricao": "Linguagem C. Completa e Descomplicada by Andre Brackes",
        "preco": 87,
        "imagem": "imagens/1.jpg",
        "categoria": "TI",
        "id": 1
    },
    {
        "nome": "HTML e CSS",
        "descricao": "HTML e CSS Projete e Construa Websites",
        "preco": 99.9,
        "imagem": "imagens/2.jpg",
        "categoria": "Informatica",
        "id": 2
    },
    {
        "nome": "Excel e VBA",
        "descricao": "Programando Excel Vba Para Leigos",
        "preco": 50,
        "imagem": "imagens/3.jpg",
        "categoria": "Informatica",
        "id": 3
    }
]

===========================================================
			/produtos/{id}

mostra apenas o produto referente ao id informado no parametro.

Exemplo de chamada: 
/produtos/2  (Médoto GET)

retorno:

{
        "nome": "HTML e CSS",
        "descricao": "HTML e CSS Projete e Construa Websites",
        "preco": 99.9,
        "imagem": "imagens/2.jpg",
        "categoria": "Informatica",
        "id": 2
}

===========================================================
			/categoria/{categoria}
mostra os produtos (livros) por uma determinada categoria

Exemplo de chamada:

/categoria/Informatica

retorno:

[
    {
        "nome": "HTML e CSS",
        "descricao": "HTML e CSS Projete e Construa Websites",
        "preco": 99.9,
        "imagem": "imagens/2.jpg",
        "categoria": "Informatica",
        "id": 2
    },
    {
        "nome": "Excel e VBA",
        "descricao": "Programando Excel Vba Para Leigos",
        "preco": 50,
        "imagem": "imagens/3.jpg",
        "categoria": "Informatica",
        "id": 3
    }
]


============================================================
			/incluirproduto  (POST)
passar o JSON conforme abaixo para incluir um novo produto e-book. METODO POST

{
	"nome": "novo livro", 
	"descricao": "criando novo livro",
	"imagem": "/images/5.jpg",
	"preco": 20,
	"categoria": "infantil"
}


============================================================

			/excluirproduto (DELETE)

passar o JSON com o id do produto para a exclusão do mesmo. METODO DELETE


{
	"id": 7
}

