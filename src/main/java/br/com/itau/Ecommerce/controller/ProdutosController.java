package br.com.itau.Ecommerce.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.itau.Ecommerce.model.Produtos;
import br.com.itau.Ecommerce.repository.ProdutosRepository;


@Controller	
public class ProdutosController {
	@Autowired
	ProdutosRepository produtosRepository;

	@RequestMapping(path="/produtos", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Produtos> MostrarProdutos() {		
		return produtosRepository.findAll();
	}


	@RequestMapping(path="/produtos/{id}", method=RequestMethod.GET)
	@ResponseBody
	public Produtos getProdutosById(@PathVariable long id) {
		return produtosRepository.findById(id);
	}

	@RequestMapping(path="/categoria/{categoria}", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Produtos> getProdutosByCategoria(@PathVariable String categoria) {
		Produtos produtos = new Produtos();
		produtos.getCategoria(); 
		System.out.println("Categoria: " + categoria);
		return produtosRepository.findByCategoria(categoria);
	}


	@RequestMapping(path="/incluirproduto", method=RequestMethod.POST)
	@ResponseBody
	public Produtos inserirProduto(@RequestBody Produtos produtos) {
		return produtosRepository.save(produtos);
	}

	@RequestMapping(path="/excluirproduto", method=RequestMethod.DELETE)
	@ResponseBody
	public void excluirProduto(@RequestBody Produtos produtos) {
		 produtosRepository.delete(produtos);
		 
	}

}
