package br.com.itau.Ecommerce.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.Ecommerce.model.Produtos;

public interface ProdutosRepository  extends CrudRepository<Produtos, Long>{

	Produtos findById(long id);
	Iterable<Produtos> findByCategoria(String categoria);

}
